This site is licensed under [[https://creativecommons.org/licenses/by-sa/4.0/][Creative Commons — Attribution-ShareAlike
4.0 International • — CC BY-SA 4.0]] if not stated otherwise.
